package faith.mom_gay.replanter.client;

import faith.mom_gay.replanter.ModConfig;
import me.shedaniel.autoconfig.AutoConfig;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropBlock;
import net.minecraft.block.NetherWartBlock;
import net.minecraft.client.MinecraftClient;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Direction;

@Environment(EnvType.CLIENT)
public class ReplanterClient implements ClientModInitializer {
    public ModConfig config;

    @Override
    public void onInitializeClient() {
        config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
        UseBlockCallback.EVENT.register((player, world, hand, block) -> {
            BlockState state = world.getBlockState(block.getBlockPos());
            if (!(state.getBlock() instanceof CropBlock) && !(state.getBlock() instanceof NetherWartBlock)) {
                return ActionResult.PASS;
            }

            if (hand != Hand.MAIN_HAND)
                return ActionResult.PASS;

            if (isCropMature(state)) {
                if (config.pickBlock) {
                    int pos = player.getInventory().indexOf(state.getBlock().asItem().getDefaultStack());
                    if (pos > -1 && pos < 9) {
                        player.getInventory().addPickBlock(state.getBlock().asItem().getDefaultStack());
                    } else if (pos > -1) {
                        MinecraftClient.getInstance().interactionManager.pickFromInventory(pos);
                    }
                }
                MinecraftClient.getInstance().interactionManager.attackBlock(block.getBlockPos(), block.getSide());
                player.swingHand(hand);
                if (player.getMainHandStack().getItem().equals(state.getBlock().asItem())) {
                    MinecraftClient.getInstance().interactionManager.interactBlock(MinecraftClient.getInstance().player, MinecraftClient.getInstance().world, hand, block.withBlockPos(block.getBlockPos().down()));
                } else if (player.getOffHandStack().getItem().equals(state.getBlock().asItem())) {
                    MinecraftClient.getInstance().interactionManager.interactBlock(MinecraftClient.getInstance().player, MinecraftClient.getInstance().world, Hand.OFF_HAND, block.withBlockPos(block.getBlockPos().down()).withSide(Direction.UP));
                    return ActionResult.CONSUME;
                }
                return ActionResult.SUCCESS;
            }

            return ActionResult.PASS;
        });
    }

    public static boolean isCropMature(BlockState state) {
        if (state.getBlock() instanceof CropBlock) {
            CropBlock crop = (CropBlock) state.getBlock();
            return crop.isMature(state);
        }
        if (state.getBlock() instanceof NetherWartBlock) {
            return state.get(Properties.AGE_3) == 3;
        }
        return false;
    }

}
